const fs = require("fs");
const http = require("http");
const uuid = require("uuid4");
const port = 8000;

const reqHandler = async (req, res) => {
  res.writeHead(200, { "content-type": "text/html" });

  let filePath;

  switch (true) {
    case req.url === "/":
      filePath = "./Responses/home.html";
      break;

    case req.url === "/html":
      filePath = "./Responses/res.html";
      break;

    case req.url === "/json":
      filePath = "./Responses/res.json";
      break;

    case req.url === "/uuid":
      res.end(`
        uuid : ${uuid()};
      `);
      return;

    case req.url === "/status":
      res.end("PLEASE APPEND THE STATUS CODE IN URL BAR ABOVE");
      return;

    case req.url === "/delay":
      res.end("PLEASE APPEND THE DELAY TIME IN URL BAR ABOVE");
      return;

    case req.url.startsWith("/status/"):
      let status = req.url.split("/");
      let statusCode = status[status.length - 1];
      res.statusCode = statusCode;
      res.writeHead(statusCode);
      res.end(`Your status code is : ${statusCode}`);
      return;

    case req.url.startsWith("/delay/"):
      let urlArr = req.url.split("/");
      let delayTime = urlArr[urlArr.length - 1];
      setTimeout(() => {
        res.end(`HERE IS YOUR RESPONSE AFTER ${delayTime} seconds`);
      }, delayTime * 1000);
      return;

    default:
      filePath = "./Responses/404.html";
  }

  fs.readFile(filePath, (err, data) => {
    if (err) {
      console.error(err);
      return res.end(err.message);
    }
    return res.end(data);
  });
};

const server = http.createServer(reqHandler);

server.listen(port, (err) => {
  if (err) console.error(err);
  else console.log("Server is up and Running on port: ", port);
});
